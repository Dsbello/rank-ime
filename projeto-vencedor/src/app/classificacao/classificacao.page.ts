import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { RankingService } from '../ranking/ranking.service';

@Component({
  selector: 'app-classificacao',
  templateUrl: './classificacao.page.html',
  styleUrls: ['./classificacao.page.scss'],
})
export class ClassificacaoPage implements OnInit {
  alunos:any[]
  
  constructor(public service: RankingService,public control:NavController) {
    this.alunos = this.service.getData();
  }
  ngOnInit() {
  }

}
