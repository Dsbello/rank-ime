import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ClassificacaoprofessorPage } from './classificacaoprofessor.page';

const routes: Routes = [
  {
    path: '',
    component: ClassificacaoprofessorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ClassificacaoprofessorPage]
})
export class ClassificacaoprofessorPageModule {}
