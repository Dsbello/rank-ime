import { Component, OnInit } from '@angular/core';
import { RankingService } from '../ranking/ranking.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-classificacaoprofessor',
  templateUrl: './classificacaoprofessor.page.html',
  styleUrls: ['./classificacaoprofessor.page.scss'],
})
export class ClassificacaoprofessorPage implements OnInit {
  alunos:any[]
  constructor(public service: RankingService,public control:NavController) {
    this.alunos = this.service.getData();
  }

  ngOnInit() {
  }

}
