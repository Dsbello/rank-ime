import { RankingPage } from './../ranking/ranking.page';
import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { NavParams,NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  username:string =""
  password:string=""
  
  constructor(public afAuth:AngularFireAuth,public control:NavController) { }

  ngOnInit() {
  }

  async login(){
    const {username,password}=this
    try {
        const res =await this.afAuth.auth.signInWithEmailAndPassword(username +'@gmail.com',password);
        alert("Login feito com sucesso!");
        this.control.navigateRoot('/ranking');
    } catch(err) {
      console.dir(err)
      if(err.code === "auth/user-not-found"){
        console.log("User not found")
        alert("User not found")
      }
    }
  }
}
