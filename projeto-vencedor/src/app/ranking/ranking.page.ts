import { NavController } from '@ionic/angular';
import { RankingService, Aluno } from './ranking.service';
import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,FormBuilder} from '@angular/forms' ;

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.page.html',
  styleUrls: ['./ranking.page.scss'],
})
export class RankingPage  {
  nomes;
  alunos:any[]
  alunos2;
  form= new FormGroup({
    'nomeDoAluno':new FormControl,
    'notaDoAluno':new FormControl

  })
  
  formAlterado=new FormGroup({
    'nomeDoAlunoAlterado':new FormControl,
    'notaDoAlunoAlterado':new FormControl
  })
  
  constructor(public service: RankingService,public control:NavController) {
    this.alunos = this.service.getData();
  }
  
  onSubmit(){
    let nome=(this.form.get('nomeDoAluno').value as string);
    let nota=(this.form.get('notaDoAluno').value as number);

    this.service.setarAluno(nome,nota);
    this.form.get('nomeDoAluno').setValue(null);
    this.form.get('notaDoAluno').setValue(null);
    
    }
  
  mudarNota(){
      let nome=(this.formAlterado.get('nomeDoAlunoAlterado').value as string);
      let nota=(this.formAlterado.get('notaDoAlunoAlterado').value as number);

    this.service.setarAluno(nome,nota);
    this.formAlterado.get('nomeDoAlunoAlterado').setValue(null);
    this.formAlterado.get('notaDoAlunoAlterado').setValue(null);
    this.control.navigateRoot('/ranking');
    
    }
  
}
