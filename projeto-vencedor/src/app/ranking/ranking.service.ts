import { Injectable } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import * as firebase from 'firebase';
import { FirebaseDatabase } from '@angular/fire';
import { AngularFireDatabase } from 'angularfire2/database'
import { HttpClientModule, HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class RankingService {
  url="https://primeiro-projeto-a3c7e.firebaseio.com/";
  
  constructor(public angularFirebase: AngularFireDatabase,public http: HttpClient,) {
  }

  
  public setarAluno(nome:string,nota:number){
      this.angularFirebase.database.ref('/Alunos/'+ nome+'/nome').set(nome);
     this.angularFirebase.database.ref('/Alunos/'+ nome+'/nota').set(nota);
     console.log(nome,nota);
    }
    public getData(){
      let alunos:Aluno[]=[]
      this.angularFirebase.database.ref('/').once('value').then(function (snapshot) {
      
     snapshot.child('/Alunos').forEach(function(childSnapshot){
       let aluno=new Aluno(childSnapshot.child('/nome').val(),childSnapshot.child('/nota').val());
       alunos.push(aluno);
     })
    
    
      })
      console.log(alunos);
      return alunos;
      }
}
export class Aluno{
  constructor(public nome:string,public nota:number){}
}