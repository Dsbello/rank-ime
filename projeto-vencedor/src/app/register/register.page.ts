import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  username:string=""
  password:string=""
  cpassword:string=""
  constructor(public afAuth: AngularFireAuth) { }

  ngOnInit() {
  }

  async register (){
    const{username,password,cpassword}=this
    if(password != cpassword){
      alert("As senhas não batem");
      return console.error("Senhas não batem")
    }
    try{
      const res =await this.afAuth.auth.createUserWithEmailAndPassword(username+'@gmail.com',password)
      console.log(res)
      alert("Cadastro realizado com sucesso! Retorne para a tela de login")  
    } catch(error){
      console.dir(error)
      if(error.code === "auth/invalid-email"){
        alert("O usuario está incorreto");
      }
      if(error.code === "Password should be at least 6 characters"){
        alert("A senha deve conter ao menos 6 caracteres");
      }
      if(error.code === "auth/email-already-in-use"){
        alert("O nome de usuario já está em uso");
      }
    }
  }
}
